package br.ufg.pos.fswm.fpb.client.controller;

import br.ufg.pos.fswm.fpb.client.modelo.Aluno;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
@Controller
public class AlunosControlador {

    @GetMapping("/alunos")
    public @ResponseBody ModelAndView getTodosAlunos() {
        RestTemplate restTemplate = new RestTemplate();
        List<Aluno> alunos = restTemplate.getForObject("http://localhost:8080/alunos", List.class);

        ModelAndView mv = new ModelAndView("/alunos");
        mv.addObject("alunos", alunos);

        return mv;
    }
}
