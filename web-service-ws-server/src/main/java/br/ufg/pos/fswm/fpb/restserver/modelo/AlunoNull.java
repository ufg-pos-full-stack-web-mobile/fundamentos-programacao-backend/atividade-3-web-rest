package br.ufg.pos.fswm.fpb.restserver.modelo;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
public class AlunoNull extends Aluno {
    public AlunoNull() {
        super(0, "Aluno não encontrado");
    }
}
