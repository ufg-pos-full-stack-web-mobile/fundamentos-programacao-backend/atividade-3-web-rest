package br.ufg.pos.fswm.fpb.restserver.modelo;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
public class Aluno {

    private int id;
    private String nome;

    public Aluno(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
