package br.ufg.pos.fswm.fpb.restserver.controllers;

import br.ufg.pos.fswm.fpb.restserver.modelo.Aluno;
import br.ufg.pos.fswm.fpb.restserver.modelo.AlunoNull;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
@RestController
@RequestMapping("/alunos")
public class AlunosCtrl {

    private List<Aluno> alunos;

    public AlunosCtrl() {
        alunos = new ArrayList<Aluno>();
        alunos.add(new Aluno(1, "Chapeuzinho"));
        alunos.add(new Aluno(2, "Lobo Mal"));
        alunos.add(new Aluno(3, "Caçador"));
    }

    @GetMapping
    public @ResponseBody List<Aluno> getTodos() {
        return alunos;
    }

    @GetMapping("/{id}")
    public @ResponseBody Aluno getAluno(@PathVariable("id") int id) {
        for (Aluno aluno : alunos) {
            if(id == aluno.getId()) {
                return aluno;
            }
        }
        return new AlunoNull();
    }
}
