package br.ufg.pos.fswm.fpb.restserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
@SpringBootApplication
public class AppInitializer {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(AppInitializer.class, args);
    }
}
